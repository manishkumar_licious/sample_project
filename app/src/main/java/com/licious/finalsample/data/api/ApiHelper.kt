package com.licious.finalsample.data.api

import com.licious.finalsample.data.model.ProductData
import retrofit2.Response

interface ApiHelper {

    suspend fun getCateList(): Response<ProductData>
}