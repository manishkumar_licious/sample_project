package com.licious.finalsample.data.api

import com.licious.finalsample.data.model.ProductData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    @GET("catalog/products/all")
    suspend fun getUsers(@Query("cat_id") cat_id: String?,
                         @Query("hub_id") hub_id: String?,
                         @Header("deviceid") deviceid: String?,
                         @Header("macid") macid: String?,
                         @Header("token") token: String?,
                         @Header("customerkey") customerkey: String?): Response<ProductData>

}