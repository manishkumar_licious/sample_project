package com.licious.finalsample.data.api

import com.licious.finalsample.data.model.ProductData
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getCateList(): Response<ProductData> = apiService.getUsers("19","1","97dcc46a80c74b31",
    "02:00:00:00:00:00","tk_40599ufkcfwm6ec","c_kb9bj8nc")

}