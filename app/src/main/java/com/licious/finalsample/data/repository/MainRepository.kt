package com.licious.finalsample.data.repository

import com.licious.finalsample.data.api.ApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getCateList() =  apiHelper.getCateList()

}