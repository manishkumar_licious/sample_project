package com.licious.finalsample.data.model

import com.squareup.moshi.Json

data class ProductData(

    @Json(name = "status")
    val status: String = "",
    @Json(name = "message")
    val message: String = "",
    @Json(name = "data")
    val data: ArrayList<DataList>
)

class  DataList(
    @Json(name = "product_master")
    val product_master: product_master,
    @Json(name = "product_merchantdising")
    val product_merchantdising: product_merchantdising
)

class  product_master(
    @Json(name = "pr_name")
    val pr_name: String = ""
)
class  product_merchantdising(
    @Json(name = "pr_image")
    val pr_image: String = ""
)