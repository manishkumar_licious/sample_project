package com.licious.hiltsample.utils

import com.licious.finalsample.data.model.ProductData

data class Resource<out T>(val status: Status, val data: ProductData?, val message: String?) {

    companion object {

        fun <T> success(data: ProductData?): Resource<T>? {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: ProductData?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: ProductData?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

    }

}