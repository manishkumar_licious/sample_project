package com.licious.hiltsample.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}