package com.licious.finalsample.ui.main.adapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.licious.finalsample.R
import com.licious.finalsample.data.model.DataList
import kotlinx.android.synthetic.main.catalog_single_row_products.view.*

class ProductListAdapter(
    private val users: ArrayList<DataList>
) : RecyclerView.Adapter<ProductListAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: DataList) {
            itemView.tvProductName.text = user.product_master.pr_name
            Glide.with(itemView.ivProductImage.context)
                .load(user.product_merchantdising.pr_image)
                .into(itemView.ivProductImage)

            itemView.tvProductName.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    Log.d("AAAMMNCCEC","Helllooo")
                    Toast.makeText(itemView.context,"clicked",Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.catalog_single_row_products, parent,
                false
            )
        )

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(users.get(position))


    }
    fun addData(list: ArrayList<DataList>) {
        users.addAll(list)
    }
}