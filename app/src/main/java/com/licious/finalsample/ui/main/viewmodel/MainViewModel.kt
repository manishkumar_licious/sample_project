package com.licious.finalsample.ui.main.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.licious.finalsample.data.model.ProductData
import com.licious.finalsample.data.repository.MainRepository
import com.licious.hiltsample.utils.NetworkHelper
import com.licious.hiltsample.utils.Resource
import kotlinx.coroutines.launch

class MainViewModel
@ViewModelInject
constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper) : ViewModel() {

    private val _users = MutableLiveData<Resource<List<ProductData>>>()
    val users: LiveData<Resource<List<ProductData>>>
        get() = _users

    init {
        fetchUsers()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            _users.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                mainRepository.getCateList().let {
                    if (it.isSuccessful) {
                        _users.postValue(Resource.success(it.body()))
                    } else _users.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _users.postValue(Resource.error("No internet connection", null))
        }
    }
}